<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<body>
				<h1>Order Information:</h1>
				<xsl:apply-templates />
				<br />
				<br />
				<b>
					Total number of items in the order:
					<xsl:value-of select="count(/Order/Item)" />
				</b>
				<br />
				<b>
					Total price of the order :
					<xsl:value-of select="sum(/Order/Item/Price)" />
				</b>	
			</body>
		</html>
	</xsl:template>

	<xsl:template match="Item">
		<b>
			<br />
			Prueba de concat:
			<xsl:value-of select="concat(./ItemName, ./Quantity) "></xsl:value-of>
			<br />

			Prueba de substring:
			<xsl:value-of select="substring(./ItemName, 1, 3)"></xsl:value-of>
			<br />

			Prueba de substring-after:
			<xsl:value-of select="substring-after(./ItemName, 'o')"></xsl:value-of>
			<br />

			Prueba de substring-before:
			<xsl:value-of select="substring-before(./ItemName, 'o')"></xsl:value-of>
			<br />

			Prueba de substring-length:
			<xsl:value-of select="string-length(./ItemName)"></xsl:value-of>
			<br />

			Prueba de minúscula:
			<xsl:value-of
				select="translate(./ItemName, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')"></xsl:value-of>
			<br />

			Prueba de cambio de letras:
			<xsl:value-of select="translate(./ItemName, 'Aa', 'Xx')"></xsl:value-of>
			<br />

			Redondeo hacia arriba si es positivo, hacia abajo si es negativo:
			<xsl:value-of select="ceiling(./Price)"></xsl:value-of>
			<br />

			Prueba de contains:
			<xsl:value-of select="contains(./ItemName, 'iPhone')"></xsl:value-of>
			<br />

			Prueba de contains2:
			<xsl:value-of select="contains(./ItemName, '\d{5}')"></xsl:value-of>
			<br />

			Prueba de paseAString:
			<xsl:value-of select="string(./ItemId)"></xsl:value-of>
			<br />

			Prueba de ConcatDateAndTime:
			<xsl:value-of select="concat(./Date,' ', ./Time)"></xsl:value-of>
			<br />

			Prueba de normalize-space:
			<xsl:value-of select="normalize-space(./Espacios)"></xsl:value-of>
			<br />

			Prueba de formateo de fecha:
			<xsl:value-of
				select="format-number(./Date, '[D]-[M]-[Y]')"></xsl:value-of>

			TESTEO DE QUANTITY
			<xsl:if test="(./Quantity) = 1 ">
				<xsl:value-of select="(./Quantity)+ 9"></xsl:value-of>
			</xsl:if>
			<xsl:if test="not(./Quantity)">
				0 (No hay stock)
			</xsl:if>

			<br />
			Prueba de ConcatDateAndTime:
			<xsl:value-of select="concat(./Date,' ', ./Time)"></xsl:value-of>
			<br />
			Prueba de normalize-space:
			<xsl:value-of select="normalize-space(./Espacios)"></xsl:value-of>
			<br />
			Prueba de FormatDate:
			<xsl:value-of
				select="concat(substring(./NumberDate, 7, 2), '-', substring(./NumberDate, 5, 2), '-', substring(./NumberDate, 1, 4))"></xsl:value-of>
			FECHA:
			<xsl:value-of select="./Date"></xsl:value-of>
			Prueba de FormatDate:
			<xsl:value-of
				select="concat(substring(./NumberDate, 7, 2), '-', substring(./NumberDate, 5, 2), '-', substring(./NumberDate, 1, 4))"></xsl:value-of>


		</b>
	</xsl:template>
</xsl:stylesheet>